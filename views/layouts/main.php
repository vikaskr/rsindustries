<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE HTML>
<html>
<head>
    <title>RS Industries</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

</head>

<body>
    <!-- page header -->
    <header class="only-color">
        <!-- header top panel -->
        <div class="page-header-top">
            <div class="grid-row clear-fix">
                <address>
                    <a href="#" class="phone-number"><i class="fa fa-phone"></i>+91-9818320647</a>
                    <a href="mailto:info@rsiapl.in" class="email"><i class="fa fa-envelope-o"></i>info@rsiapl.in</a>
                </address>
                <div class="header-top-panel">
                    <!-- <a href="" class="fa fa-facebook"></a> -->
                    <!-- <a href="page-login.html" class="fa fa-user login-icon"></a>
                    <div id="top_social_links_wrapper">
                        <div class="share-toggle-button"><i class="share-icon fa fa-share-alt"></i></div>
                        <div class="cws_social_links"><a href="https://plus.google.com/" class="cws_social_link" title="Google +"><i class="share-icon fa fa-google-plus" style="transform: matrix(0, 0, 0, 0, 0, 0);"></i></a><a href="http://twitter.com/" class="cws_social_link" title="Twitter"><i class="share-icon fa fa-twitter"></i></a><a href="http://facebook.com" class="cws_social_link" title="Facebook"><i class="share-icon fa fa-facebook"></i></a><a href="http://dribbble.com" class="cws_social_link" title="Dribbble"><i class="share-icon fa fa-dribbble"></i></a></div>
                    </div>
                    <a href="#" class="search-open"><i class="fa fa-search"></i></a>
                    <form action="#" class="clear-fix">
                        <input type="text" placeholder="Search" class="clear-fix">
                    </form> -->
                    
                </div>
            </div>
        </div>
        <!-- / header top panel -->
        <!-- sticky menu -->
        <div class="sticky-wrapper">
            <div class="sticky-menu">
                <div class="grid-row clear-fix">
                    <!-- logo -->
                    <a href="/" class="logo">
                        <img src="/img/rsi.png"  data-at2x="img/logo@2x.png" alt>
                        <h1>RS Industries Pvt. Ltd.</h1>
                    </a>
                    <!-- / logo -->
                    <nav class="main-nav">
                        <ul class="clear-fix">
                            <li>
                                <a href="/" class="active">Home</a>
                                <!-- sub menu -->
                               <!--  <ul>
                                    <li><a href="/" class="active">Full-Width Slider</a></li>
                                    <li><a href="index-fullscreen.html">Full-Screen Slider</a></li>
                                    <li><a href="index-bg-video.html">Video Slider</a></li>
                                </ul> -->
                                <!-- / sub menu -->
                            </li>
                            <li class="megamenu">
                                <a href="/site/about">About Us</a>
                                <!-- sub mega menu -->
                                <!-- <ul class="clear-fix">
                                    <li><div class="header-megamenu">Pages</div>
                                        <ul>
                                            <li><a href="page-about-us.html">About Us</a></li>
                                            <li><a href="page-our-staff.html">Our Staff</a></li>
                                            <li><a href="page-services.html">Services</a></li>
                                            <li><a href="page-full-width.html">Full-Width Page</a></li>
                                            <li><a href="page-left-sidebar.html">Page Left Sidebar</a></li>
                                            <li><a href="page-right-sidebar.html">Page Right Sidebar</a></li>
                                            <li><a href="page-double-sidebars.html">Double Sidebars</a></li>
                                            <li><a href="page-faq.html">Faq Page</a></li>
                                            <li><a href="page-sitemap.html">SiteMap</a></li>
                                            <li><a href="page-404.html">404 Page</a></li>
                                        </ul>
                                    </li>
                                    <li><div class="header-megamenu">Content</div>
                                        <ul>
                                            <li><a href="content-elements.html">Content Elements</a></li>
                                            <li><a href="page-content-typography.html">Typography</a></li>
                                            <li><a href="page-pricing-plans.html">Pricing Plans</a></li>
                                            <li><a href="page-login.html">Login</a></li>
                                            
                                        </ul>
                                        <img src="http://placehold.it/250x150" alt>
                                    </li>
                                    <li><div class="header-megamenu">Portfolio</div>
                                        <ul>
                                            <li><a href="portfolio-one-column.html">One Column</a></li>
                                            <li><a href="portfolio-two-columns.html">Two Columns</a></li>
                                            <li><a href="portfolio-three-columns.html">Three Columns</a></li>
                                            <li><a href="portfolio-four-columns.html">Four Columns</a></li>
                                            <li><a href="portfolio-gallery.html">Gallery</a></li>
                                            <li><a href="portfolio-filtered.html">Filtered</a></li>
                                        </ul>
                                        <img src="http://placehold.it/250x150" alt>
                                    </li>
                                    <li><div class="header-megamenu">Blog</div>
                                        <ul>
                                            <li><a href="blog-default.html">Default</a></li>
                                            <li><a href="blog-two-columns.html">Two Columns</a></li>
                                            <li><a href="blog-three-columns.html">Three Columns</a></li>
                                            <li><a href="blog-fullwidth.html">Full Width</a></li>
                                            <li><a href="blog-post.html">Blog Post</a></li>
                                        </ul>
                                        <img src="http://placehold.it/250x150" alt>
                                    </li>
                                </ul> -->
                                <!-- / sub mega menu -->
                            </li>
                            <li>
                                <a href="/site/products">Products</a>
                                <!-- sub menu -->
                                <!-- <ul>
                                   
                                    <li><a href="event-calendar.html">Events Calendar</a></li>
                                    <li><a href="events-single-item.html">Events Single Item</a></li>
                                </ul> -->
                                <!-- / sub menu -->
                            </li>
                            <li>
                                <a href="/site/branch">Our Braches</a>
                                <!-- sub menu -->
                               <!--  <ul>
                                    <li><a href="courses-grid.html">Courses grid</a></li>
                                    <li><a href="courses-list.html">Courses list</a></li>
                                    <li><a href="courses-single-item.html">Courses single item</a></li>
                                </ul> -->
                                <!-- / sub menu -->
                            </li>
                            <li>
                                <a href="/site/clients">Clients</a>
                                <!-- sub menu -->
                               <!--  <ul>
                                    <li><a href="/site/blog-details">Blog Details</a></li>
                                    <li><a href="event-calendar.html">Events Calendar</a></li>
                                    <li><a href="events-single-item.html">Events Single Item</a></li>
                                </ul> -->
                                <!-- / sub menu -->
                            </li>
                           <!--  <li>
                                <a href="/site/blogs">News</a> -->
                                <!-- sub menu -->
                               <!--  <ul>
                                    <li><a href="/site/blog-details">Blog Details</a></li>
                                    <li><a href="shop-single-product.html">Single Product</a></li>
                                    <li><a href="shop-checkout.html">Checkout</a></li>
                                    <li><a href="shop-cart.html">Shop Cart</a></li>
                                </ul> -->
                                <!-- / sub menu -->
                            </li>
                            <li>
                                <a href="/site/contact">Contact Us</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <!-- sticky menu -->
    </header>

<?= $content ?>

<footer>
        <div class="grid-row marginb">
            <div class="grid-col-row clear-fix">
                <section class="grid-col grid-col-4 footer-latest">
                    <h2 class="corner-radius">Quick Links</h2>
                        <ul class="clear-fix">
                        <li>
                            <a href="/" target="_blank" class="colorw"> Home</a>
                        </li>
                        <li>
                            <a href="/site/about" target="_blank" class="colorw"> About Us</a>
                        </li>
                        <li>
                            <a href="/site/products" target="_blank" class="colorw"> Our Products</a>
                        </li>
                        <li>
                            <a href="/site/braches" target="_blank" class="colorw"> Our Braches</a>
                        </li>
                        <li>
                            <a href="/site/clients" target="_blank" class="colorw"> Our Clients</a>
                        </li>
                        <li>
                            <a href="/site/contact" target="_blank" class="colorw"> Contact Us</a>
                        </li>
                    </ul>
                </section>
                <section class="grid-col grid-col-4 footer-about">
                    <h2 class="corner-radius">Address: All Branches</h2>
                    <address>
                       <a href="www.sample.com" class="address"> Plot Number 118,Sector 6, IMT Manesar, Gurugram, Haryana.</a><br/><br/>
                       <a href="www.sample.com" class="address"> Plot No 6 , Street No 5 , Surat Nagar Industrial Area, Gurugram, Haryana.</a><br/><br/>
                       <a href="www.sample.com" class="address"> Khasra No 2366/4655/280 ,Surat Nagar Industrial Area,Dhanuapur Road, Gurugram, Haryana</a><br/><br/>
                       <a href="www.sample.com" class="address"> Khasra No 6319/175 , Rajendra Park Road ,Near Hooda Factory, Gurugram, Haryana.</a>
                    </address>
                    <div class="footer-social">
                        <a href="" class="fa fa-facebook"></a>
                    </div>
                </section>
                <section class="grid-col grid-col-4 footer-contact-form">
                    <h2 class="corner-radius">Contact Us</h2>
                            <p> <i class="fa fa-phone" aria-hidden="true"></i> +91-9818320647</br></p>
                            <p><i class="fa fa-phone" aria-hidden="true"></i> 0124.- 6900008<br/></p>
                     </h4>
                                <p><i class="fa fa-envelope" aria-hidden="true"></i>  info@rsiapl.in<br/></p>
                                <p><i class="fa fa-envelope" aria-hidden="true"></i>  purchase@rsiapl.in<br/></p>
                                <p><i class="fa fa-envelope" aria-hidden="true"></i>  director@rsiapl.in</p>
                     </h4>
                </section>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="grid-row clear-fix">
                <div class="copyright center">RS Industries<span></span> 2017 . All Rights Reserved <a href="http://makeubig.com/" target="_blank">  Degisned By : MakeUBig</a></div>
                <nav class="footer-nav">
                    <ul class="clear-fix">
                        <li>
                            
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
