<div class="page-title">
	<div class="grid-row">
		<h1>About Us</h1>
		<nav class="bread-crumb">
			<a href="/">Home</a>
			<i class="fa fa-long-arrow-right"></i>
			<a href="#">About Us</a>
		</nav>
	</div>
</div>

<div class="page-content grid-row">
<main>
	<section class="clear-fix just">
		<h2>Who we Are</h2>
		<p><b>Started in 2003, R S Industries</b> has grown from a sheet metal component supplier to an complete filter manufacturer for automotive industry.

R S Industries, owing to its commitment to quality and the support of its clients have expanded exponentially from a small production facility to two fully operational factories in Gurgaon and coming up unit at Kushkhera in Rajasthan.

R S Industries boosts of almost complete backwards integration through in-house facility for paper coating and processing , sheet metal fabrication,  filter foam rings,  injection moulding etc.

The fully equipped and operational labs ensure thorough quality check and value enhancement  for our esteemed clients.  
</p>
		<div class="block-overflow">
			<h3>Our Mission</h3>
			<div class="columns-row">
				<div class="columns-col columns-col-12">
					<ul class="check-list">
						<li>To thoroughly understand the needs of our customers and continuously surpass their expectations by delivering exceptionally.
</li>
						<li>To give each & every event a different Meaning, Identity and Vision with true professionalism to chart the roads of quality and economy
</li>
						<li>To focus on our associate's needs and requirements so that they can reach their target consumers through us. 
</li>
						<li>Our customization provides best possible services in most economical and efficient way. 
</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<hr class="divider-color" />
	<section>
		<div class="grid-row cent">
			
			<q><b>Our Vision</b><br/>
				To be the most preferred services providers for all automotive manufacturers.
		</q>
		</div>
	</section><br/>
<hr class="divider-color" /><br/>
<div class="grid-col grid-col-5">
	<main>
		<section class="clear-fix">
			<h2>Mohit Sharma </h2>
			<p class="fs-18"><strong>(Owner of Rsi Group companies) </strong></p>
			<img src="http://placehold.it/260x290" data-at2x="http://placehold.it/260x290" class="border-img img-float-left" alt>
			<p>Mohit Sharma, Director.<br/><br/> He has over 14 years of experience in the auto ancillary industry. He has knowledge of marketing and production functions of the auto ancillary industry. He holds a degree in Mechanical Engineering.</p>
			
		</section>
		
	</main>
<hr class="divider-color" />
</div><br/>
<section>
	<div class="grid-row clear-fix">
		<div class="grid-col-row">
			<div class="grid-col grid-col-6">
			</div>	
		</div>
	</div>
</section>
	<section>
		<div class="grid-row clear-fix">
		<h2 class="margtop cent">Our Activities</h2><br/>
			<div class="grid-col-row">
				<div class="grid-col grid-col-2"><ul class="check-list"></ul></div>
				<div class="grid-col grid-col-6">
					<div class="owl-carousel full-width-slider">
						<div class="gallery-item picture">
							<img src="/img/banner.jpg" data-at2x="/img/banner.jpg" class="border-img img-float-left" width="470" alt="" style="height: 300px">
						</div>
					</div>
				</div>
				<div class="grid-col grid-col-3">
					<ul class="check-list">
						<li>Injection Moulding Components</li>

						<li>Sheet Metal & Turning Components</li>

						<li>Automotive Filters</li>

						<li>Woven and Non Woven Felt Media</li>

						<li>Foam and EPDM Components</li>

						<li>In House Tool Room , Design and Development</li>
					</ul>
				</div>
				
				
			</div>
		</div><br/>
	</section>
	<hr class="divider-color" /><br/>
</main></div>