<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'RS Industries';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="tp-banner-container">
        
                    <img src="/img/banner2.jpg" data-lazyload="/img/banner2.jpg" alt="" style="height:420px; width: 100%;">
                   
    </div>
    <!-- / revolution slider -->
    <hr class="divider-color">
    <!-- content -->
    <div id="home" class="page-content padding-none">
        <!-- section -->
        <section class="padding-section mart">
            <div class="grid-row clear-fix">
                <h2 class="center-text">Our Products</h2>
                <div class="grid-col-row">
                    <div class="grid-col grid-col-4">
                        <!-- course item -->
                        <div class="course-item">
                            <div class="course-hover">
                                <img src="/img/injection.jpg" data-at2x="/img/injection.jpg" alt="" style="height: 280px">
                                <div class="hover-bg bg-color-1"></div>
                                <a href="/site/products/#product1" target="_blank">Learn More</a>
                            </div>
                            <div class="course-name clear-fix">
                               
                            <h3><a href="#">Injection Moulding Components</a></h3>
                                </div>
                        </div>
                        <!-- / course item -->
                    </div>
                    <div class="grid-col grid-col-4">
                        <!-- course item -->
                        <div class="course-item">
                            <div class="course-hover">
                                <img src="/img/2.jpg" data-at2x="/img/2.jpg" alt="">
                                <div class="hover-bg bg-color-2"></div>
                                <a href="/site/products/#product2" target="_blank">Learn More</a>
                            </div>
                            <div class="course-name clear-fix">
                               
                            <h3><a href="#">Sheet Metal & Turning Components</a></h3>
                                </div>
                           
                        </div>
                        <!-- / course item -->
                    </div>
                    <div class="grid-col grid-col-4">
                        <!-- course item -->
                        <div class="course-item">
                            <div class="course-hover">
                                <img src="/img/auto.jpg" data-at2x="/img/auto.jpg" alt="" style="height: 280px">
                                <div class="hover-bg bg-color-3"></div>
                                <a href="/site/products/#product3" target="_blank">Learn More</a>
                            </div>
                            <div class="course-name clear-fix">
                                
                            <h3><a href="#">Automotive Filters</a></h3>
                                </div>
                           
                        </div>
                        <!-- course item -->
                    </div>

                </div>
            </div>
        </section>
            <div class="grid-row clear-fix">
                <div class="grid-col-row">
                    <div class="grid-col grid-col-4">
                        <!-- course item -->
                        <div class="course-item">
                            <div class="course-hover">
                                <img src="/img/felt.jpg" data-at2x="/img/felt.jpg" alt="" style="height: 280px">
                                <div class="hover-bg bg-color-1"></div>
                                <a href="/site/products/#product4" target="_blank">Learn More</a>
                            </div>
                            <div class="course-name clear-fix">
                               
                            <h3><a href="#">Woven And Non Woven Felt Media</a></h3>
                                </div>
                        </div>
                        <!-- / course item -->
                    </div>
                    <div class="grid-col grid-col-4">
                        <!-- course item -->
                        <div class="course-item">
                            <div class="course-hover">
                                <img src="/img/form.jpg" data-at2x="/img/form.jpg" alt="" style="height: 280px">
                                <div class="hover-bg bg-color-2"></div>
                                <a href="/site/products/#product5" target="_blank">Learn More</a>
                            </div>
                            <div class="course-name clear-fix">
                               
                            <h3><a href="#">Foam and EPDM Components</a></h3>
                                </div>
                           
                        </div>
                        <!-- / course item -->
                    </div>                    
                </div>
            </div>
       </div>
        
        <section class="fullwidth-background padding-section">
            <div class="grid-row clear-fix">
                <div class="grid-col-row">
                    <div class="grid-col grid-col-8 clear-fix">
                        <h1>Our Quality Policy</h1>
                        <p class="just">RS Industries (Auto ) Pvt. Ltd.  Is committed to complete satisfaction of our clients through consistent product quality and continual improvement in all areas because we believe tat Quality comes by Habit not by Cost. <br/><br/>

                        “An extra effort to improve a little bit with each and every count can do Wonders”.<br/>

                        <h3>Quality Objective :</h3>

                          1. CONTINUAL IMPROVEMENT IN QUALITY & DELIVERY.<br/>
                          2. IMPROVEMENT IN  PROCESS PERFORMANCE.<br/>
                          3. REGULAR EFFORT  FOR COST REDUCTION<br/>
                        </p>
                    </div>
                    <div class="grid-col grid-col-4">
                       <img src="/img/Untitled.png">
                    </div>
                </div>
            </div>
        </section>
        <!-- / section -->
        <!-- paralax section -->
<!--         <div class="parallaxed">
            <div class="parallax-image" data-parallax-left="0.5" data-parallax-top="0.3" data-parallax-scroll-speed="0.5">
                <img src="img/parallax.png" alt="">

            </div>
            <div class="them-mask bg-color-1"></div>
            <div class="grid-row">
                <div class="grid-col-row clear-fix">
                    <div class="grid-col grid-col-3 alt">
                        <div class="counter-block">
                            <i class="flaticon-book1"></i>
                            <div class="counter" data-count="356">0</div>
                            <div class="counter-name">Courses</div>
                        </div>
                    </div>
                    <div class="grid-col grid-col-3 alt">
                        <div class="counter-block">
                            <i class="flaticon-multiple"></i>
                            <div class="counter" data-count="4781">0</div>
                            <div class="counter-name">Students</div>
                        </div>                          
                    </div>
                    <div class="grid-col grid-col-3 alt">
                        <div class="counter-block">
                            <i class="flaticon-pencil"></i>
                            <div class="counter" data-count="41">0</div>
                            <div class="counter-name">Lections</div>
                        </div>
                    </div>
                    <div class="grid-col grid-col-3 alt">
                        <div class="counter-block last">
                            <i class="flaticon-calendar"></i>
                            <div class="counter" data-count="120">0</div>
                            <div class="counter-name">Events</div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- / paralax section -->
        <!-- section -->
        <!-- <section class="fullwidth-background padding-section">
            <div class="grid-row">
                <h1 class="center-text">Our Facility</h1> -->
                <!-- time line -->
                <!-- <div class="time-line">
                    <div class="line-element">
                        <div class="action">
                            <div class="action-block">
                                <span><i class="flaticon-magnifier"></i></span>
                                <div class="text">
                                    <h3>Search your course</h3>
                                    <p>Lorem ipsum  dolor sit amet, consectetuer adipisc ing elit. Aenean commodo ligula.</p>
                                </div>
                            </div>
                        </div>
                        <div class="level">
                            <div class="level-block">Step 1</div>
                        </div>
                    </div>
                    <div class="line-element color-2">
                        <div class="level">
                            <div class="level-block">Step 2</div>
                        </div>
                        <div class="action">
                            <div class="action-block">
                                <span><i class="flaticon-computer"></i></span>
                                <div class="text">
                                    <h3>Take A Sample Lesson</h3>
                                    <p>Aenean massa. Cum sociis nato que penatibus et magnis dis par turient montes.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="line-element color-3">
                        <div class="action">
                            <div class="action-block">
                                <span><i class="flaticon-shopping"></i></span>
                                <div class="text">
                                    <h3>Purchase the Course</h3>
                                    <p>Donec quam felis, ultricies nec, pellent esque eu, pretium quis, sem. Nulla conse massa.</p>
                                </div>
                            </div>
                        </div>
                        <div class="level">
                            <div class="level-block">Step 3</div>
                        </div>
                    </div>
                </div> -->
                <!-- / time line -->
            <!-- </div>
        </section> -->
        <!-- / paralax section -->
    <hr class="divider-color" />
        <section>
            <h2 class="cent">Our Facilities</h2>
            <div class="grid-row clear-fix">
                <div class="grid-col-row">
                    <div class="grid-col grid-col-6">
                    <h4>Sheet Metal Shop</h4>
                    <ul class="check-list">
                        <li>Capacity  :  5 Ton – 100 Ton</li>
                        <li>No of Machine :  35   Nos </li>
                    </ul>  
                        <div class="owl-carousel full-width-slider">
                        <div class="gallery-item picture">
                            <img src="/img/1.jpg" data-at2x="/img/1.jpg" class="border-img img-float-left" alt>
                        </div>
                    </div>
                    </div>
                    <div class="grid-col grid-col-6">
                    <h4>Weld Shop </h4>
                    <ul class="check-list">
                        <li>Projection Welding</li>
                        <li>Spot Welding</li>
                    </ul>  
                        <div class="owl-carousel full-width-slider">
                        <div class="gallery-item picture">
                            <img src="/img/weld.jpg" data-at2x="/img/weld.jpg" class="border-img img-float-left" alt>
                        </div>
                    </div>
                    </div>
                </div>
                <hr class="divider-color" />

                <div class="grid-col-row">
                    <div class="grid-col grid-col-6 margtop2">
                    <h4 class="cent">Paper Coating Plant</h4>
                        <div class="owl-carousel full-width-slider">
                        <div class="gallery-item picture">
                            <img src="/img/paper2.jpg" data-at2x="/img/paper2.jpg" class="border-img img-float-left" alt>
                        </div>
                    </div>
                    </div>
                    <div class="grid-col grid-col-6 margtop2">
                        <h4 class="cent">Paper Pleating Plant</h4>
                        <div class="owl-carousel full-width-slider">
                        <div class="gallery-item picture">
                            <img src="/img/paper.jpg" data-at2x="/img/paper.jpg" class="border-img img-float-left" alt>
                        </div>
                    </div>
                    </div>
                </div>
                <hr class="divider-color" />
                 <div class="grid-col-row">
                    <div class="grid-col grid-col-6 margtop2">
                    <h4>Injection Moulding</h4>
                    <ul class="check-list">
                        <li>110 Ton, 180 Ton </li>
                        <li>260 Ton, 320 Ton </li>
                    </ul> 
                        <div class="owl-carousel full-width-slider">
                        <div class="gallery-item picture">
                            <img src="/img/omega.jpg" data-at2x="/img/omega.jpg" class="border-img img-float-left" alt>
                        </div>
                    </div>
                    </div>
                   
                </div>
            </div>
        </section>
        <!-- / section -->
        <!-- google map -->
        <div class="wow fadeInUp">
           <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3510.4453835237564!2d76.92458531446006!3d28.375612852414502!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d3dd0935f9f8b%3A0x2f34e3d68af3a324!2sR+S+Industries+(Auto+)+Pvt.+Ltd!5e0!3m2!1sen!2sin!4v1514443762907" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        <!-- / google map -->
    </div>