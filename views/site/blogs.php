<div class="page-title">
			<div class="grid-row">
				<h1>Blog</h1>
				<!-- bread crumb -->
				<nav class="bread-crumb">
					<a href="/">Home</a>
					<i class="fa fa-long-arrow-right"></i>
					<a href="#">Blogs</a>
				</nav>
				<!-- / bread crumb -->
			</div>
		</div>
	</header>
	<!-- / page header -->
	<!-- content -->
	<div class="page-content grid-row">
		<!-- main content -->
		<main>
			<div class=" grid-col-row clear-fix">
				<div class="grid-col grid-col-4">
					<!-- blog post -->
					<div class="blog-post">
						<article>
							<div class="post-info">
								<div class="date-post"><div class="day">26</div><div class="month">Feb</div></div>
								<div class="post-info-main">
									<div class="author-post">by Orkun Gusel</div>
								</div>
								<div class="comments-post"><i class="fa fa-comment"></i> 3</div>
							</div>
							<div class="blog-media picture">
								<div class="hover-effect"></div>
								<div class="link-cont">
									<a href="/site/blog-details" target="_blank" class="cws-left fancy fa fa-link"></a>
									<a href="/site/blog-details" target="_blank" class="fancy fa fa-search"></a>
									
								</div>
								<img src="http://placehold.it/370x270" data-at2x="http://placehold.it/370x270" class="columns-col-12" alt>
							</div>
							<h3>Donec mollis magna quis</h3>
							<p>Donec sollicitudin lacus in felis luctus blandit. Ut hendrerit mattis justo at suscipit. <a href="/site/blog-details" target="_blank"></a></p>
							<div class="tags-post">
							<a href="#" rel="tag">Online Materials</a>
							<a href="#" rel="tag">Featured, Education</a>
						</div>

						</article>
					</div>
					<!-- / blog post -->
					<hr class="divider-big" />
					<!-- blog post -->
					<!-- blog post -->
					<div class="blog-post"><article>
						<div class="post-info">
							<div class="date-post"><div class="day">15</div><div class="month">Feb</div></div>
							<div class="post-info-main">
								<div class="author-post">by Orkun Gusel</div>
							</div>
							<div class="comments-post"><i class="fa fa-comment"></i> 3</div>
						</div>
						<h3>Vestibulum volutpat est neque</h3>
						<p>Praesent ornare sollicitudin magna. Vestibulum volutpat est neque, in rutrum mi interdum quis. Praesent dictum rhoncus luctus. </p>
						<div class="tags-post">
							<a href="#" rel="tag">Materials</a><!-- 
						 --><a href="#" rel="tag">Featured</a>
						</div>
					</article></div>
					<!-- blog post -->
					<!-- blog post -->
					<hr class="divider-big" />
					<!-- blog post -->
					<div class="blog-post"><article>
						<div class="post-info">
							<div class="date-post"><div class="day">12</div><div class="month">Feb</div></div>
							<div class="post-info-main">
								<div class="author-post">by Orkun Gusel</div>
							</div>
							<div class="comments-post"><i class="fa fa-comment"></i> 5</div>
						</div>
						<div class="video-player">
							<iframe src="https://www.youtube.com/embed/PvoUgT1mlfA"></iframe>
						</div>
						<h3>Class aptent taciti sociosqu ad litora.</h3>
						<p>Donec sollicitudin lacus in felis luctus blandit. Ut hendrerit mattis justo at suscipit. Praesent sagittis magna nec neque viverra lobortis. </p>
						<div class="tags-post">
							<a href="#" rel="tag">Materials</a><!-- 
						 --><a href="#" rel="tag">Featured</a>
						</div>
					</article></div>
					<!-- / blog post -->
					<hr class="divider-big" />
				</div>
				<div class="grid-col grid-col-4">
					<!-- blog post -->
					<div class="blog-post"><article>
						<div class="post-info">
							<div class="date-post"><div class="day">12</div><div class="month">Feb</div></div>
							<div class="post-info-main">
								<div class="author-post">by Orkun Gusel</div>
							</div>
							<div class="comments-post"><i class="fa fa-comment"></i> 0</div>
						</div>
						<div class="container-audio">
							<audio controls preload="metadata">
								<source src="audio/audio.mp3" type="audio/mpeg">
							</audio>
						</div>
						<h3>Sed lobortis consectetur quam</h3>
						<p>Nunc sed urna est. Suspendisse pulvinar, arcu vel volutpat imperdiet, nisi dolor viverra est, non vehicula lacus lectus vel lacus. </p>
						<div class="tags-post">
							<a href="#" rel="tag">Materials</a><!-- 
						 --><a href="#" rel="tag">Featured</a>
						</div>
					</article></div>
					<!-- / blog post -->
					<hr class="divider-big" />
					<!-- blog post -->
					<div class="blog-post"><article>
						<div class="post-info">
							<div class="date-post"><div class="day">05</div><div class="month">Feb</div></div>
							<div class="post-info-main">
								<div class="author-post">by Orkun Gusel</div>
							</div>
							<div class="comments-post"><i class="fa fa-comment"></i> 5</div>
						</div>
						<div class="owl-carousel full-width-slider">
							<div class="gallery-item picture">
								<img src="http://placehold.it/370x270" data-at2x="http://placehold.it/370x270" alt>
							</div>
							<div class="gallery-item picture">
								<img src="http://placehold.it/370x270" data-at2x="http://placehold.it/370x270" alt>
							</div>
							<div class="gallery-item picture">
								<img src="http://placehold.it/370x270" data-at2x="http://placehold.it/370x270" alt>
							</div>
						</div>
						<h3>Donec mollis magna quis urna.</h3>
						<p>Etiam viverra pellentesque dui a feugiat.eros sem in dui. In eu sagittis metus. Proin consectetur. </p>
						<div class="tags-post">
							<a href="#" rel="tag">Materials</a><!-- 
						 --><a href="#" rel="tag">Featured</a>
						</div>
					</article></div>
					<!-- / blog post -->
					<hr class="divider-big" />
					<!-- blog post -->
					<!-- blog post -->
					<div class="blog-post"><article>
						<div class="post-info">
							<div class="date-post"><div class="day">12</div><div class="month">Feb</div></div>
							<div class="post-info-main">
								<div class="author-post">by Orkun Gusel</div>
							</div>
							<div class="comments-post"><i class="fa fa-comment"></i> 0</div>
						</div>
						<audio controls>
							<source src="audio/audio.mp3" type="audio/mpeg">
						</audio>
						<h3>Sed lobortis consectetur quam</h3>
						<p>Nunc sed urna est. Suspendisse pulvinar, arcu vel volutpat imperdiet, nisi dolor viverra est, non vehicula lacus lectus vel lacus. </p>
						<div class="tags-post">
							<a href="#" rel="tag">Materials</a><!-- 
						 --><a href="#" rel="tag">Featured</a>
						</div>
					</article></div>
					<!-- / blog post -->
					<!-- / blog post -->
					<hr class="divider-big" />
				</div>
				<div class="grid-col grid-col-4">
					<!-- blog post -->
					<div class="blog-post"><article>
						<div class="post-info">
							<div class="date-post"><div class="day">12</div><div class="month">Feb</div></div>
							<div class="post-info-main">
								<div class="author-post">by Orkun Gusel</div>
							</div>
							<div class="comments-post"><i class="fa fa-comment"></i> 5</div>
						</div>
						<div class="video-player">
							<iframe src="https://www.youtube.com/embed/rZsH88zNxRw"></iframe>
						</div>
						<h3>Class aptent taciti sociosqu</h3>
						<p>Donec sollicitudin lacus in felis luctus blandit. Ut hendrerit mattis justo at suscipit. <a href="/site/blog-details" target="_blank"></a></p>
						<div class="tags-post">
							<a href="#" rel="tag">Materials</a><!-- 
						 --><a href="#" rel="tag">Featured</a>
						</div>
					</article></div>
					<!-- / blog post -->
					<hr class="divider-big" />
					<!-- blog post -->
					<div class="blog-post"><article>
						<div class="post-info">
							<div class="date-post"><div class="day">01</div><div class="month">Feb</div></div>
							<div class="post-info-main">
								<div class="author-post">by Orkun Gusel</div>
							</div>
							<div class="comments-post"><i class="fa fa-comment"></i> 2</div>
						</div>
						<div class="quotes clear-fix">
							<div class="quote-avatar-author clear-fix"><img src="http://placehold.it/60x60" data-at2x="http://placehold.it/60x60" alt><div class="author-info">Jasica Doe<br/><span>Writer</span></div></div>
							<q><b>Vestibulum et metus</b><br/>
								Praesent sagittis magna nec neque viverra lobortis. Quisque tincidunt tortor ac nisl elementum.
							</q>
						</div>
						<br/>
						<div class="tags-post">
							<a href="#" rel="tag">Materials</a><!-- 
						 --><a href="#" rel="tag">Featured</a>
						</div>
					</article></div>
					<!-- / blog post -->
					<hr class="divider-big" />
					<!-- blog post -->
					<div class="blog-post"><article>
						<div class="post-info">
							<div class="date-post"><div class="day">05</div><div class="month">Feb</div></div>
							<div class="post-info-main">
								<div class="author-post">by Orkun Gusel</div>
							</div>
							<div class="comments-post"><i class="fa fa-comment"></i> 5</div>
						</div>
						<div class="owl-carousel full-width-slider">
							<div class="gallery-item picture">
								<img src="http://placehold.it/370x270" data-at2x="http://placehold.it/370x270" alt>
							</div>
							<div class="gallery-item picture">
								<img src="http://placehold.it/370x270" data-at2x="http://placehold.it/370x270" alt>
							</div>
							<div class="gallery-item picture">
								<img src="http://placehold.it/370x270" data-at2x="http://placehold.it/370x270" alt>
							</div>
						</div>
						<h3>Aenean congue nisl vel purus</h3>
						<p>Donec sollicitudin lacus in felis luctus blandit. Ut hendrerit mattis justo at suscipit. <a href="/site/blog-details" target="_blank"></a></p>
						<div class="tags-post">
							<a href="#" rel="tag">Materials</a><!-- 
						 --><a href="#" rel="tag">Featured</a>
						</div>
					</article></div>
					<!-- / blog post -->
					<hr class="divider-big" />
				</div>
			</div>
		</main>
		<!-- / main content -->
	</div>