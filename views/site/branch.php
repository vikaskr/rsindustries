	<div class="page-title">
	<div class="grid-row">
		<h1>Our Braches</h1>
		<nav class="bread-crumb">
			<a href="/">Home</a>
			<i class="fa fa-long-arrow-right"></i>
			<a href="#">Our Braches</a>
		</nav>
	</div>
    </div>

	<section>
		<div class="grid-row clear-fix">
			<div class="grid-col-row">
				<div class="grid-col grid-col-6">
					<div class="owl-carousel full-width-slider">
						<div class="gallery-item picture">
							<img src="/img/branch1.png" data-at2x="/img/branch1.png" alt>
						</div>
					</div>
				</div>
				<div class="grid-col grid-col-6">
					<h2>R S Industries ( Auto ) Pvt. Ltd </h2>
					<p>Plot No 118 , Sector  - 6 , IMT Maneser, Gurgaon. </p><ul class="check-list">
						<h2>Operations :</h2>

						<li>Injection Moulding.</li>
						<li>Assembly Line</li>
						<li>Testing Lab</li>
						<li>Tool Room</li>
					</ul>
				</div>
				
			</div>
		</div>
	</section>
	<hr class="divider-color" />
		<section>
		<div class="grid-row clear-fix">
			<div class="grid-col-row">
				<div class="grid-col grid-col-6">
					<h2>R S Industries</h2>
					<p>Plot No 6 , Street No 5 , Surat Nagar Industrial Area, Gurgaon. </p><ul class="check-list">
						<h2>Operations :</h2>

						<li>Air Cleaner Assembly</li>
						<li>Oil Filter Assembly</li>
						<li>Sheet Metal </li>
					</ul>
				</div>
				<div class="grid-col grid-col-6">
					<div class="owl-carousel full-width-slider">
						<div class="gallery-item picture">
							<img src="/img/branch2.png" data-at2x="/img/branch2.png" alt>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</section>	<hr class="divider-color" /><section>
		<div class="grid-row clear-fix">
			<div class="grid-col-row">
				<div class="grid-col grid-col-6">
					<div class="owl-carousel full-width-slider">
						<div class="gallery-item picture">
							<img src="/img/branch3.png" data-at2x="/img/branch3.png" alt>
						</div>
					</div>
				</div><div class="grid-col grid-col-6">
					<h2>Rmax Automotive System</h2>
					<p>Khasra No 2366/4655/280 ,Surat Nagar Industrial Area,Dhanuapur Road Gurgaon.
					</p><ul class="check-list">
						<h2>Operations :</h2>

						<li>PU Foam Cutting</li>
						<li>Felt Cutting</li>
						<li>Assembly Line</li>
						<li>Filter Paper Coating and Pleating</li>
					</ul>
				</div>
				
			</div>
		</div>
	</section>	<hr class="divider-color" /><section>
		<div class="grid-row clear-fix">
			<div class="grid-col-row"><div class="grid-col grid-col-6">
					<h2>R S Automotive Pvt. Ltd  </h2>
					<p>Khasra No 6319/175 , Rajendra Park Road ,Near Hooda Factory, Gurgaon </p>
					<ul class="check-list">
						<h2>Operations :</h2>

						<li>Sheet Metal Components</li>
						<li>Projection & Spot Welding</li>
						<li>Tool Room</li>
						<li>Perforation</li>
					</ul>
				</div>
				<div class="grid-col grid-col-6">
					<div class="owl-carousel full-width-slider">
						<div class="gallery-item picture">
							<img src="/img/branch4.png" data-at2x="/img/branch4.png" alt>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</section>	<hr class="divider-color" />