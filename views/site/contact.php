<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
        <div class="page-title">
            <div class="grid-row">
                <h1>Contact Us</h1>
                <!-- bread crumb -->
                <nav class="bread-crumb">
                    <a href="/">Home</a>
                    <i class="fa fa-long-arrow-right"></i>
                    <a href="#">Contact Us</a>
                </nav>
                <!-- / bread crumb -->
            </div>
        </div>
        <!-- page title -->
    <!-- / page header -->
    <!-- page content -->
    <div class="page-content woocommerce">
        <!-- map -->
       
        <!-- / map -->
        <!-- contact form section -->
        <div class="grid-row clear-fix">
            <div class="grid-col-row">
                <div class="grid-col grid-col-8">
                    <section>
                        <h2>Enquiry Form</h2>
                       
                          <form action="mailto:director@rsiapl.in" method="post" enctype="text/plain" target="_blank">

                            <label for="fname">Name</label>
                            <input type="text" id="fname" name="name" placeholder="Enter Your Name" required="">

                            <label for="lname">Email</label>
                            <input type="text" id="lname" name="email" placeholder="Enter Your E-mail" required="">

                            <label for="lname">Mobile</label>
                            <input type="text" id="lname" name="mobile" placeholder="Enter Your Phone" required="">

                            <label for="lname">Subject</label>
                            <input type="text" id="lname" name="mobile" placeholder="Write Something" required="">           <br/>

                            <input type="submit" value="Send" class="cws-button border-radius">

                          </form>
                       
                    </section>
                </div>
                <div class="grid-col grid-col-4 widget-address">
                    <section>
                        <h2>Our Offices</h2>
                            <p><strong class="fs-18">Contact Person: Mohit Sharma, MD</strong><br/>
                                <a href="tel:305-333552"><i class="fa fa-phone" aria-hidden="true"></i> +91-9818320647</a><br/>
                                <a href="tel:305-333552"><i class="fa fa-phone" aria-hidden="true"></i> 0124.- 6900008</a>
                            </p>
                        <address><br/>
                            <p><strong class="fs-18">Address: All Branches</strong><br/><i class="fa fa-map-marker" aria-hidden="true"></i>  Plot Number 118,Sector 6, IMT Manesar, Gurugram, Haryana.</p>
                            <i class="fa fa-map-marker" aria-hidden="true"></i>  Plot No 6 , Street No 5 , Surat Nagar Industrial Area, Gurugram, Haryana.</p>
                            <i class="fa fa-map-marker" aria-hidden="true"></i>  Khasra No 2366/4655/280 ,Surat Nagar Industrial Area,Dhanuapur Road, Gurugram, Haryana.</p>
                            <i class="fa fa-map-marker" aria-hidden="true"></i>  Khasra No 6319/175 , Rajendra Park Road ,Near Hooda Factory, Gurugram, Haryana.</p><br/>

                            <!-- <p><strong class="fs-18">E-mail:</strong><br/>
                                <a href="mailto:uni@domain.com"><i class="fa fa-envelope" aria-hidden="true"></i>  info@rsiapl.in</a><br/>
                                <a href="mailto:uni@domain.com"><i class="fa fa-envelope" aria-hidden="true"></i>  purchase@rsiapl.in</a><br/>
                                <a href="mailto:uni@domain.com"><i class="fa fa-envelope" aria-hidden="true"></i>  director@rsiapl.in</a>
                            </p> -->
                        </address>
                    </section>
                </div>
            </div>
        </div>
         <div class="container clear-fix">
            <div class="map wow fadeInUp">
               <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3510.4453835237564!2d76.92458531446006!3d28.375612852414502!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d3dd0935f9f8b%3A0x2f34e3d68af3a324!2sR+S+Industries+(Auto+)+Pvt.+Ltd!5e0!3m2!1sen!2sin!4v1514443762907" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
        <!-- / contact form section -->
    </div>