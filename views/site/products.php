

<div class="page-title">
	<div class="grid-row">
		<h1>Our Products</h1>
		<nav class="bread-crumb">
			<a href="/">Home</a>
			<i class="fa fa-long-arrow-right"></i>
			<a href="#">Products</a>
		</nav>
	</div>
</div>
<br/>

	<section id="product1"><br/><br/><br/>
		<div class="grid-row clear-fix">
			<h2 class="margtop">1. Injection Moulding Components</h2><br/>

			<div class="grid-col-row">
				<div class="grid-col grid-col-4">
					<ul class="check-list">
						<h4>Injection Moulding :</h4>

						<li>110 Ton</li> 
						<li>180 Ton X 2</li>
						<li>260 Ton</li> 
						<li>320 Ton  X 2</li>
						<li>360 Ton</li>  
						<li>Ordered  450 , 550 , 650 ,600</li> 
					</ul>
					<a href="/site/contact" target="_blank" class="cws-button border-radius alt icon-right">Enquiry<i class="fa fa-angle-right"></i></a>

				</div>
				<div class="grid-col grid-col-8">
					<div class="owl-carousel full-width-slider">
						<div class="gallery-item picture">
							<img src="/img/injection.jpg" data-at2x="/img/injection.jpg" class="border-img img-float-left" width="440" alt>
						</div>
					</div>
				</div>
				
			</div>
		</div><br/>
		<div class="grid-row clear-fix">
			<div class="grid-col-row">
				<div class="grid-col grid-col-12">
					<img src="/img/injecting1.jpg" data-at2x="/img/injecting1.jpg" class="border-img img-float-left" width="240" height="110" alt>
					<img src="/img/injecting2.jpg" data-at2x="/img/injecting2.jpg" class="border-img img-float-left" width="240" height="110" alt>
					<img src="/img/injecting3.jpg" data-at2x="/img/injecting3.jpg" class="border-img img-float-left" width="240" height="110" alt>
				<!-- 	<img src="/img/img4.jpg" data-at2x="/img/img4.jpg" class="border-img img-float-left" width="240" height="110" alt> -->
				</div>	
			</div>
		</div>			
	</section>
	<hr class="divider-color" /><br/>
	<section id="product2"><br/><br/><br/>
		<div class="grid-row clear-fix">
			<h2 class="margtop">2. Sheet Metal & Turning Components</h2><br/>
			<div class="grid-col-row">
				<div class="grid-col grid-col-8">
					<div class="owl-carousel full-width-slider">
						<div class="gallery-item picture">
							<img src="/img/1.jpg" data-at2x="/img/1.jpg" class="border-img img-float-left" width="470" alt>
						</div>
					</div>
				</div>
				<div class="grid-col grid-col-4">
					<ul class="check-list">

						<li>Sheet Metal Assemblies</li>
						<li>Wire Mesh, Rings</li>
						<li>Spring Clips</li>					
					</ul>
					<h4>Sheet Metal Shop</h4>
					<ul class="check-list">
					<li>Capacity  :  5 Ton – 250 Ton</li>
					</ul>
					<a href="/site/contact" target="_blank" class="cws-button border-radius alt icon-right">Enquiry<i class="fa fa-angle-right"></i></a>

				</div>
				
			</div>
		</div><br/>
		<div class="grid-row clear-fix">
			<div class="grid-col-row">
				<div class="grid-col grid-col-12">
					<img src="/img/img23.png" data-at2x="/img/img23.png" class="border-img img-float-left" width="240" height="110" alt>
					<img src="/img/img24.jpg" data-at2x="/img/img24.jpg" class="border-img img-float-left" width="240" height="110" alt>
					<img src="/img/img25.jpg" data-at2x="/img/img25.jpg" class="border-img img-float-left" width="240" height="110" alt>
				</div>	
			</div>
		</div>			
	</section>
	<hr class="divider-color" /><br/>
		<section id="product3"><br/><br/><br/>
		<div class="grid-row clear-fix">
			<h2 class="margtop">3. Automotive Filters</h2><br/>
			<div class="grid-col-row">
				<div class="grid-col grid-col-4">
					<ul class="check-list">
						<li>Air Filters</li>
						<li>Oil Filters</li>
						<li>Foam Filter</li>
						<li>Fuel Filter</li>				
					</ul>
					<a href="/site/contact" target="_blank" class="cws-button border-radius alt icon-right">Enquiry<i class="fa fa-angle-right"></i></a>
				</div>
				<div class="grid-col grid-col-8">
					<div class="owl-carousel full-width-slider">
						<div class="gallery-item picture">
							<img src="/img/auto.jpg" data-at2x="/img/auto.jpg" class="border-img img-float-left" width="470" alt>
						</div>
					</div>
				</div>
				
			</div>&nbsp;
				<div class="grid-row">
					<div class="grid-col-row">
					<div class="grid-col grid-col-12">
					
					<img src="/img/img31.jpg" data-at2x="/img/img31.jpg" class="border-img img-float-left" width="250" height="150" alt>
					<img src="/img/img32.jpg" data-at2x="/site/img32.jpg" class="border-img img-float-left" width="250" height="150" alt>
					<img src="/img/filter2.jpg" data-at2x="/site/filter2.png" class="border-img img-float-left" width="250" height="150" alt>
					<img src="/img/filter1.jpg" data-at2x="/site/filter1.png" class="border-img img-float-left" width="250" height="150" alt>
					
					</div>	
					</div>
				</div>	
		</div>
	
	</section>
	<hr class="divider-color" /><br/>
	<section id="product4"><br/><br/><br/>
		<div class="grid-row clear-fix">
			<h2 class="margtop">4. Woven And Non Woven Felt Media</h2><br/>

			<div class="grid-col-row">
				<div class="grid-col grid-col-4">
					<ul class="check-list">
						<li>Non Woven Felt</li>
						<li>Woven Filter Media</li>
						<li>Non Woven Filter Back</li>
					</ul>
					<a href="/site/contact" target="_blank" class="cws-button border-radius alt icon-right">Enquiry<i class="fa fa-angle-right"></i></a>

				</div>
				<div class="grid-col grid-col-8">
					<div class="owl-carousel full-width-slider">
						<div class="gallery-item picture">
							<img src="/img/felt.jpg" data-at2x="/img/felt.jpg" class="border-img img-float-left" width="440" alt>
						</div>
					</div>
				</div>
				
			</div>
		</div><br/>
		<div class="grid-row clear-fix">	<br/>		
	</section>
	<hr class="divider-color" /><br/>
	<section id="product5"><br/><br/><br/>
		<div class="grid-row clear-fix">
		<h2 class="margtop">5. Foam and EPDM Components</h2><br/>
			<div class="grid-col-row">
				<div class="grid-col grid-col-8">
					<div class="owl-carousel full-width-slider">
						<div class="gallery-item picture">
							<img src="/img/form.jpg" data-at2x="/img/form.jpg" class="border-img img-float-left" width="470" alt>
						</div>
					</div>
				</div>
				<div class="grid-col grid-col-4">
					<ul class="check-list">
						<li>PU Foam</li> 
						<li>EPDM Gaskets</li>
						<li>EVA Foam Components</li>
						<li>PU Bonded Foam Parts</li>
					</ul>
					<a href="/site/contact" target="_blank" class="cws-button border-radius alt icon-right">Enquiry<i class="fa fa-angle-right"></i></a>
				</div>
			</div>
			<div class="grid-row clear-fix margtop3">
				<div class="grid-col-row">
					<div class="grid-col grid-col-12">
						<img src="/img/img57.jpg" data-at2x="/img/img57.jpg" class="border-img img-float-left" width="300" height="110" alt>
						<img src="/img/img56.jpg" data-at2x="/img/img56.jpg" class="border-img img-float-left" width="300" height="110" alt>
						<img src="/img/img55.jpg" data-at2x="/img/img55.jpg" class="border-img img-float-left" width="300" height="110" alt>
						<img src="/img/img58.jpg" data-at2x="/img/img58.jpg" class="border-img img-float-left" width="300" height="110" alt>
						<img src="/img/img59.jpg" data-at2x="/img/img59.jpg" class="border-img img-float-left" width="300" height="110" alt>
						<img src="/img/img60.jpg" data-at2x="/img/img60.jpg" class="border-img img-float-left" width="300" height="110" alt>
					</div>	
				</div>
			</div>	
		</div>
		</div>
	</section>
	<hr class="divider-color" />
<section id="product3"><br/><br/><br/>
		<div class="grid-row clear-fix">
			<h2 class="margtop">6. Filter Paper</h2><br/>
<!-- 			<div class="grid-col-row">
				<div class="grid-col grid-col-4">
					<ul class="check-list">
						<li>Air Filters</li>
						<li>Oil Filters</li>
						<li>Foam Filter</li>
						<li>Fuel Filter</li>				
					</ul>
					<a href="/site/contact" target="_blank" class="cws-button border-radius alt icon-right">Enquiry<i class="fa fa-angle-right"></i></a>
				</div>
				<div class="grid-col grid-col-8">
					<div class="owl-carousel full-width-slider">
						<div class="gallery-item picture">
							<img src="/img/auto.jpg" data-at2x="/img/auto.jpg" class="border-img img-float-left" width="470" alt>
						</div>
					</div>
				</div>
				
			</div>&nbsp; -->
				<div class="grid-row">
					<div class="grid-col-row">
					<div class="grid-col grid-col-12">
					
					<img src="/img/paer1.jpg" data-at2x="/img/paer1.jpg" class="border-img img-float-left" width="250" height="150" alt>
					<img src="/img/pap2.jpg" data-at2x="/site/pap2.jpg" class="border-img img-float-left" width="250" height="150" alt>
					<img src="/img/paper2.jpg" data-at2x="/site/paper2.jpg" class="border-img img-float-left" width="250" height="150" alt>
					
					</div>	
					</div>
				</div>	
		</div>
	
	</section>
	<hr class="divider-color" />
