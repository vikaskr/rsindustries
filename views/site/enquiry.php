<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
        <div class="page-title">
            <div class="grid-row">
                <h1>Contact Us</h1>
                <!-- bread crumb -->
                <nav class="bread-crumb">
                    <a href="/">Home</a>
                    <i class="fa fa-long-arrow-right"></i>
                    <a href="#">Contact Us</a>
                </nav>
                <!-- / bread crumb -->
            </div>
        </div>
        <!-- page title -->
    <!-- / page header -->
    <!-- page content -->
    <div class="page-content woocommerce">
        <!-- map -->
       
        <!-- / map -->
        <!-- contact form section -->
        <div class="grid-row clear-fix">
            <div class="grid-col-row">
                <div class="grid-col grid-col-8">
                    <section>
                        <h2>Contact us</h2>
                        <div class="widget-contact-form">
                            <!-- contact-form -->
                            <div class="info-boxes error-message alert-boxes error-alert" id="feedback-form-errors">
                                <strong>Attention!</strong>
                                <div class="message"></div>
                            </div>
                            <div class="email_server_responce"></div>
                            <form action="php/contacts-process.php" method="post" class="contact-form alt clear-fix">
                                <input type="text" name="name" value="" size="40" placeholder="Your Name" aria-invalid="false" aria-required="true">
                                <input type="text" name="name" value="" size="40" placeholder="Company Name" aria-invalid="false">
                                <input type="text" name="email" value="" size="40" placeholder="Email ID" aria-required="true">
                                <input type="text" name="mobile" value="" size="40" placeholder="Mobile No." aria-required="true" aria-required="true">
                                <input type="text" name="subject" value="" size="40" placeholder="Subject" aria-invalid="false" aria-required="true">
                                <textarea name="message"  cols="40" rows="3" placeholder="Your Message" aria-invalid="false" aria-required="true"></textarea>
                                <input type="submit" value="Send" class="cws-button border-radius alt">
                            </form>
                            <!--/contact-form -->
                        </div>
                    </section>
                </div>
                <div class="grid-col grid-col-4 widget-address">
                    <section>
                        <h2>Our Offices</h2>
                        <address>
                            <p>Donec sollicitudin lacus in felis luctus blandit. Ut hendrerit mattis justo at suscipit. Etiam id faucibus augue, sit amet ultricies nisi.</p>
                            <p><strong class="fs-18">Address:</strong><br/>250 Biscayne Blvd. (North) 11st Floor<br/>New World Tower Miami, Florida 33148</p>
                            <p><strong class="fs-18">Phone number:</strong><br/>
                                <a href="tel:305-333552">(305)333-5522</a><br/>
                                <a href="tel:305-333552">(305)333-5522</a>
                            </p>
                            <p><strong class="fs-18">E-mail:</strong><br/>
                                <a href="mailto:uni@domain.com">uni@domain.com</a><br/>
                                <a href="mailto:uni@domain.com">sales@your-site.com</a>
                            </p>
                        </address>
                    </section>
                </div>
            </div>
        </div>
         <div class="container clear-fix">
            <div class="map wow fadeInUp">
               <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3498.357421947667!2d77.13103431446893!3d28.738743686018072!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d0110e3aaeae5%3A0x5a3d03d4e45aff1!2sMakeUBig!5e0!3m2!1sen!2sin!4v1513325967257" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
        <!-- / contact form section -->
    </div>