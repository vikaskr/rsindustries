<div class="page-title">
	<div class="grid-row">
		<h1>Our Clients</h1>
		<nav class="bread-crumb">
			<a href="/">Home</a>
			<i class="fa fa-long-arrow-right"></i>
			<a href="#">Clients</a>
		</nav>
	</div>
</div>
<section>	
	<div class="grid-row clear-fix">
			<div class="grid-col-row">
				<div class="grid-col grid-col-12">
					<img src="/img/cl1.png" data-at2x="/img/cl1.png" class="border-img img-float-left" width="250" height="110" alt>
					<img src="/img/cl2.png" data-at2x="/img/cl2.png" class="border-img img-float-left" width="250" height="110" alt>
					<img src="/img/cl8.png" data-at2x="/img/cl8.png" class="border-img img-float-left" width="250" height="110" alt>
					<img src="/img/cl4.png" data-at2x="/img/cl4.png" class="border-img img-float-left" width="250" height="110" alt>
				</div>	
			</div>
		</div>			
</section>
<section>	
	<div class="grid-row clear-fix">
			<div class="grid-col-row">
				<div class="grid-col grid-col-12">
					<img src="/img/cl5.png" data-at2x="/img/cl5.png" class="border-img img-float-left" width="250" height="110" alt>
					<img src="/img/cl6.png" data-at2x="/img/cl6.png" class="border-img img-float-left" width="250" height="110" alt>
					<img src="/img/cl7.png" data-at2x="/img/cl7.png" class="border-img img-float-left" width="250" height="110" alt>
					<img src="/img/cl9.png" data-at2x="/img/cl9.png" class="border-img img-float-left" width="250" height="110" alt>
				</div>	
			</div>
		</div>			
</section>
<section>	
	<div class="grid-row clear-fix">
			<div class="grid-col-row">
				<div class="grid-col grid-col-12">
					<img src="/img/cl11.png" data-at2x="/img/cl11.png" class="border-img img-float-left" width="250" height="110" alt>
					<img src="/img/cl12.png" data-at2x="/img/cl12.png" class="border-img img-float-left" width="250" height="110" alt>
					<!-- <img src="/img/cl2.png" data-at2x="/img/cl2.png" class="border-img img-float-left" width="250" height="110" alt> -->
					<img src="/img/cl10.png" data-at2x="/img/cl10.png" class="border-img img-float-left" width="250" height="110" alt>
				</div>	
			</div>
		</div>			
</section>
