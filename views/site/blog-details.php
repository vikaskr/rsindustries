<div class="page-title">
			<div class="grid-row">
				<h1>Blog</h1>
				<!-- dread crumb -->
				<nav class="bread-crumb">
					<a href="index.html">Home</a>
					<i class="fa fa-long-arrow-right"></i>
					<a href="">Blog</a>
					<i class="fa fa-long-arrow-right"></i>
					<a href="">Blog Details</a>
				</nav>
				<!-- / dread crumb -->
			</div>
		</div>
	</header>
	<!-- / page header -->
	<!-- content -->
	<div class="grid-row">
		<div class="page-content grid-col-row clear-fix">
			<div class="grid-col grid-col-9">
				<!-- main content -->
				<main>
					<!-- blog post -->
					<div class="blog-post"><article>
						<div class="post-info">
							<div class="date-post"><div class="day">26</div><div class="month">Feb</div></div>
							<div class="post-info-main">
								<div class="author-post">by Orkun Gusel</div>
							</div>
							<div class="comments-post"><i class="fa fa-comment"></i> 3</div>
						</div>
						<div class="blog-media picture">
							<div class="hover-effect"></div>
							<div class="link-cont">
								<a href="blog-post.html" class="cws-left fancy fa fa-link"></a>
								<a href="http://placehold.it/870x426" class="fancy fa fa-search"></a>
								<a href="#" class="cws-right fa fa-heart"></a>
							</div>
							<img src="http://placehold.it/870x426" data-at2x="http://placehold.it/870x426" alt>
						</div>
						<h3>Donec mollis magna quis urna convallisquis</h3>
						<p>Ut hendrerit mattis justo at suscipit. Praesent sagittis magna nec neque viverra lobortis. Proin eleifend neque venenatis facilisis cursus. Cras lobortis consequat lorem a porta. Sed condimentum erat non leo euismod, non tristique lectus elementum. Donec posuere dignissim nulla. Morbi vel molestie massa, vitae scelerisque ligula. Proin euismod tortor rutrum purus porta imperdiet. Pellentesque vitae metus in lorem posuere sagittis.</p>
						<div class="quotes clear-fix">
							<div class="quote-avatar-author clear-fix"><img src="http://placehold.it/60x60" data-at2x="http://placehold.it/60x60" alt><div class="author-info">Jasica Doe<br/><span>Writer</span></div></div>
							<q><b>Vestibulum et metus a tellus sagittis</b><br/>
								Praesent sagittis magna nec neque viverra lobortis. Quisque tincidunt tortor ac nisl elementum, a congue dui vestibulum Sed nisl nisl, faucibus non eros ac, posuere pulvinar sem. Quisque volutpat tortor nec malesuada ullamcorper donec a elit non elit vehicula fermentum.
							</q>
						</div><br/>
						<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.</p>
						<div class="tags-post">
							<a href="#" rel="tag">Online Materials</a><!-- 
						 --><a href="#" rel="tag">Featured, Education</a>
						</div>
					</article></div>
					<!-- blog post -->
					<hr class="divider-color" />
					<!-- comments for post -->
					<div class="comments">
						<div id="comments">
							<div class="comment-title">Comments <span>(3)</span></div>
							<ol class="commentlist">
								<li class="comment">
									<div class="comment_container clear">
										<img src="http://placehold.it/70x70" data-at2x="http://placehold.it/70x70" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>John Doe</strong>
												<time datetime="2016-06-07T12:14:53+00:00">/ 2015.06.25 12:12:14</time>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
											<a class="button reply" href="#"><i class="fa fa-rotate-left"></i> Reply</a>
										</div>
									</div>
								</li>
								<li class="comment">
									<div class="comment_container second clear-fix">
										<img src="http://placehold.it/70x70" data-at2x="http://placehold.it/70x70" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>Tom Doe</strong>
												<time datetime="2016-06-07T12:14:53+00:00">/ 2015.06.25 12:12:14</time>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
											<a class="button reply" href="#"><i class="fa fa-rotate-left"></i> Reply</a>
										</div>
									</div>
								</li>
								<li class="comment">
									<div class="comment_container clear">
										<img src="http://placehold.it/70x70" data-at2x="http://placehold.it/70x70" alt="" class="avatar">
										<div class="comment-text">
											<p class="meta">
												<strong>John Doe</strong>
												<time datetime="2016-06-07T12:14:53+00:00">/ 2015.06.25 12:12:14</time>
											</p>
											<div class="description">
												<p>Vestibulum id nisl a neque malesuada hendrerit. Mauris ut porttitor nunc, ut volutpat nisl. Nam ullamcorper ultricies metus vel ornare. Vivamus tincidunt erat in mi accumsan, a sollicitudin risus</p>
											</div>
											<a class="button reply" href="#"><i class="fa fa-rotate-left"></i> Reply</a>
										</div>
									</div>
								</li>
							</ol>
						</div>
					</div>
					<!-- / comments for post -->
					<hr class="divider-color" />
					<div class="leave-reply">
						<div class="title">Leave a Reply</div>
						<form class="message-form clear-fix">
							<p class="message-form-author">
								<input id="author" name="author" type="text" value="" size="30" aria-required="true" placeholder="Your name">
							</p>
							<p class="message-form-subject">
								<input id="subject" name="subject" type="text" value="" size="30" aria-required="true" placeholder="Subject">
							</p>
							<p class="message-form-message">
								<textarea id="message" name="message" cols="45" rows="8" aria-required="true" placeholder="Description"></textarea>
							</p>
							<p class="form-submit rectangle-button green medium">
								<input class="cws-button border-radius alt" name="submit" type="submit" id="submit" value="Submit">
							</p>
						</form>
					</div>
				</main>
				<!-- / main content -->
			</div>
			<div class="grid-col grid-col-3 sidebar">
				<!-- widget search -->
				<aside class="widget-search">
					<form method="get" class="search-form" action="#">
						<label>
							<span class="screen-reader-text">Search for:</span>
							<input type="search" class="search-field" placeholder="Search" value="" name="s" title="Search for:">
						</label>
						<input type="submit" class="search-submit" value="GO">
					</form>
				</aside>
				<!--/ widget search -->
				<!-- widget categories -->
				<aside class="widget-categories">
					<h2>Categories</h2>
					<hr class="divider-big" />
					<ul>
						<li class="cat-item cat-item-1 current-cat"><a href="#">Software Training<span> (2) </span></a></li>
						<li class="cat-item cat-item-1 current-cat"><a href="#">Developing Mobile Apps<span> (6) </span></a></li>
						<li class="cat-item cat-item-1 current-cat"><a href="#">Arvchitecture and Built <span> (12) </span></a></li>
						<li class="cat-item cat-item-1 current-cat"><a href="#">Management and Business <span> (14) </span></a></li>
						<li class="cat-item cat-item-1 current-cat"><a href="#">Basic Cooking Techniq ues <span> (7) </span></a></li>
						<li class="cat-item cat-item-1 current-cat"><a href="#">Starting a Startup<span> (51) </span></a></li>
						<li class="cat-item cat-item-1 current-cat"><a href="#">Information Technology <span> (34) </span></a></li>
					</ul>
				</aside>
				<!-- widget categories -->
				<!-- widget recent post -->
				<aside class="widget-post">
					<h2>Recent Posts</h2>
					<div class="carousel-nav">
						<div class="carousel-button">
							<div class="prev"><i class="fa fa-angle-left"></i></div><!-- 
						 --><div class="next"><i class="fa fa-angle-right"></i></div>
						</div>
					</div>
					<hr class="divider-big" />
					<div class="owl-carousel widget-carousel">
						<div>
							<article class="clear-fix">
								<img src="http://placehold.it/60x60" data-at2x="http://placehold.it/60x60" alt>
								<h4>Lorem ipsum dolor</h4>
								<p>Sed pharetra lorem ut dolor dignissim, sit amet pretium tortor mattis...</p>
							</article>
							<article class="clear-fix">
								<img src="http://placehold.it/60x60" data-at2x="http://placehold.it/60x60" alt>
								<h4>Lorem ipsum dolor</h4>
								<p>Sed pharetra lorem ut dolor dignissim, sit amet pretium tortor mattis...</p>
							</article>
						</div>
						<div>
							<article class="clear-fix">
								<img src="http://placehold.it/60x60" data-at2x="http://placehold.it/60x60" alt>
								<h4>Lorem ipsum dolor</h4>
								<p>Sed pharetra lorem ut dolor dignissim, sit amet pretium tortor mattis...</p>
							</article>
							<article class="clear-fix">
								<img src="http://placehold.it/60x60" data-at2x="http://placehold.it/60x60" alt>
								<h4>Lorem ipsum dolor</h4>
								<p>Sed pharetra lorem ut dolor dignissim, sit amet pretium tortor mattis...</p>
							</article>
						</div>
					</div>
				</aside>
				<!-- widget recent post -->
				<!-- widget recent comments -->
				<aside class="widget-comments">
					<h2>Recent Comments</h2>
					<hr class="divider-big" />
					<div class="comments">
						<div class="comment">
							<div class="header-comments">
								<div class="date">22.04.14 /</div>
								<div class="author">Michael Lawson</div>
							</div>
							<p>Donec ut velit varius, sodales velit ac, aliquet purus. Fusce nec nisl</p>
						</div>
						<div class="comment">
							<div class="header-comments">
								<div class="date">19.04.14 /</div>
								<div class="author">Steven Granger</div>
							</div>
							<p>Donec ut velit varius, sodales velit ac, aliquet purus. Fusce nec nisl</p>
						</div>
						<div class="comment">
							<div class="header-comments">
								<div class="date">14.04.14 /</div>
								<div class="author">Mark Blackwood</div>
							</div>
							<p>Donec ut velit varius, sodales velit ac, aliquet purus. Fusce nec nisl</p>
						</div>
					</div>
				</aside>
				<!--/ widget recent comments -->
				<!-- widget tag cloud -->
				<aside class="widget-tag">
					<h2>Tag Cloud</h2>
					<hr class="divider-big margin-bottom" />
					<div class="tag-cloud">
						<a href="#" rel="tag">Daily</a>,
						<a href="#" rel="tag">Design</a>,
						<a href="#" rel="tag">Illustration</a>,
						<a href="#" rel="tag">Label</a>,
						<a href="#" rel="tag">Photo</a>,
						<a href="#" rel="tag">Pofessional</a>,
						<a href="#" rel="tag">Show</a>,
						<a href="#" rel="tag">Sound</a>,
						<a href="#" rel="tag">Sounds</a>,
						<a href="#" rel="tag">Tv</a>,
						<a href="#" rel="tag">Video</a>
					</div>
					<hr class="margin-top" />
				</aside>
				<!-- / widget tag cloud -->
				<!-- widget subscribe -->
				<aside class="widget-subscribe">
					<h2>Follow & Subscribe</h2>
					<hr class="divider-big margin-bottom" />
					<div><a href="#" class="fa fa-twitter"></a><a href="" class="fa fa-skype"></a><a href="" class="fa fa-google-plus"></a><a href="" class="fa fa-rss"></a><a href="" class="fa fa-youtube"></a></div>
				</aside>
				<!-- / widget subscribe -->
			</div>
		</div>
	</div>