<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Compare Brokers';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="cd-products-comparison-table container">
		<header>
			<h2>Background</h2>
      	</header>

		<div class="cd-products-table">
			<div class="features">
				<div class="top-info">Discount Brokers Compare</div>
				<ul class="cd-features-list">
					<li>Incorporation’s year</li>
					<li>Type of Broker</li>
					<li>Account Type</li>
					<li>Paisa Power Classic</li>
					<li>Supported Exchanges</li>
					<li>Member of (NSDL/CDSL)</li>
				</ul>
			</div> <!-- .features -->
			
			<div class="cd-products-wrapper">
				<ul class="cd-products-columns">
					

					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Brokers Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							<li>47.6 inches</li>
							<li>800Hz</li>
						</ul>
					</li> <!-- .product -->

					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Brokers Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							<li>47.6 inches</li>
							<li>800Hz</li>
						</ul>
					</li> <!-- .product -->

					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Brokers Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							<li>47.6 inches</li>
							<li>800Hz</li>
						</ul>
					</li> <!-- .product -->
					
				</ul> <!-- .cd-products-columns -->
			</div> <!-- .cd-products-wrapper -->
			
			
		</div> <!-- .cd-products-table -->
	</section> <!-- .cd-products-comparison-table -->
	<section class="cd-products-comparison-table container">
		<header>
			<h2>Brokerage</h2>
		</header>

		<div class="cd-products-table">
			<div class="features">
				<div class="top-info">Discount Brokers Compare</div>
				<ul class="cd-features-list">
					<li>Incorporation’s year</li>
					<li>Type of Broker</li>
					<li>Account Type</li>
					<li>Paisa Power Classic</li>
					<li>Supported Exchanges</li>
					<li>Member of (NSDL/CDSL)</li>
					<li>Member of (NSDL/CDSL)</li>
				</ul>
			</div> <!-- .features -->
			
			<div class="cd-products-wrapper">
				<ul class="cd-products-columns">
					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Broker Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							<li>47.6 inches</li>
							<li>800Hz</li>
							<li>800Hz</li>
						</ul>
					</li> <!-- .product -->

					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Brokers Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							<li>47.6 inches</li>
							<li>800Hz</li>
							<li>800Hz</li>
						</ul>
					</li> <!-- .product -->

					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Brokers Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							<li>47.6 inches</li>
							<li>800Hz</li>
							<li>800Hz</li>
						</ul>
					</li> <!-- .product -->

			
				</ul> <!-- .cd-products-columns -->
			</div> <!-- .cd-products-wrapper -->
			
		</div> <!-- .cd-products-table -->
	</section> <!-- .cd-products-comparison-table -->
	<section class="cd-products-comparison-table container">
		<header>
			<h2>Stozk Broker Fees / Charges</h2>
		</header>

		<div class="cd-products-table">
			<div class="features">
				<div class="top-info">Discount Brokers Compare</div>
				<ul class="cd-features-list">
					<li>Incorporation’s year</li>
					<li>Type of Broker</li>
					<li>Account Type</li>
					<li>Paisa Power Classic</li>
					<li>Supported Exchanges</li>
					<li>Member of (NSDL/CDSL)</li>
				</ul>
			</div> <!-- .features -->
			
			<div class="cd-products-wrapper">
				<ul class="cd-products-columns">
					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Broker Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							<li>47.6 inches</li>
							<li>800Hz</li>
						</ul>
					</li> <!-- .product -->

					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Brokers Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							<li>47.6 inches</li>
							<li>800Hz</li>
						</ul>
					</li> <!-- .product -->

					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Brokers Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							<li>47.6 inches</li>
							<li>800Hz</li>
						</ul>
					</li> <!-- .product -->


				</ul> <!-- .cd-products-columns -->
			</div> <!-- .cd-products-wrapper -->
			
			
		</div> <!-- .cd-products-table -->
	</section> <!-- .cd-products-comparison-table -->
	<section class="cd-products-comparison-table container">
		<header>
			<h2>Taxes</h2>
	
		</header>

		<div class="cd-products-table">
			<div class="features">
				<div class="top-info">Discount Brokers Compare</div>
				<ul class="cd-features-list">
					<li>Incorporation’s year</li>
					<li>Type of Broker</li>
					<li>Account Type</li>
					<li>Paisa Power Classic</li>
					<li>Supported Exchanges</li>
					
				</ul>
			</div> <!-- .features -->
			
			<div class="cd-products-wrapper">
				<ul class="cd-products-columns">
					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Broker Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							<li>47.6 inches</li>
							
						</ul>
					</li> <!-- .product -->

					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Brokers Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							<li>47.6 inches</li>
							
						</ul>
					</li> <!-- .product -->

					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Brokers Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							<li>47.6 inches</li>
						</ul>
					</li> <!-- .product -->

				</ul> <!-- .cd-products-columns -->
			</div> <!-- .cd-products-wrapper -->
			
			</div> <!-- .cd-products-table -->
	</section> <!-- .cd-products-comparison-table -->
		<section class="cd-products-comparison-table container">
		<header>
			<h2>Transaction Charges</h2>
		</header>

		<div class="cd-products-table">
			<div class="features">
				<div class="top-info">Discount Brokers Compare</div>
				<ul class="cd-features-list">
					<li>Incorporation’s year</li>
					<li>Type of Broker</li>
					<li>Account Type</li>
					<li>Paisa Power Classic</li>
					<li>Supported Exchanges</li>
					<li>Member of (NSDL/CDSL)</li>
					<li>Member of (NSDL/CDSL)</li>
				</ul>
			</div> <!-- .features -->
			
			<div class="cd-products-wrapper">
				<ul class="cd-products-columns">
					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Broker Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							<li>47.6 inches</li>
							<li>800Hz</li>
							<li>800Hz</li>
						</ul>
					</li> <!-- .product -->

					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Brokers Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							<li>47.6 inches</li>
							<li>800Hz</li>
							<li>800Hz</li>
						</ul>
					</li> <!-- .product -->

					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Brokers Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							<li>47.6 inches</li>
							<li>800Hz</li>
							<li>800Hz</li>
						</ul>
					</li> <!-- .product -->

				</ul> <!-- .cd-products-columns -->
			</div> <!-- .cd-products-wrapper -->
			
			</div> <!-- .cd-products-table -->
	</section> <!-- .cd-products-comparison-table -->	<section class="cd-products-comparison-table container">
		<header>
			<h2>Margins</h2>

	
		</header>

		<div class="cd-products-table">
			<div class="features">
				<div class="top-info">Discount Brokers Compare</div>
				<ul class="cd-features-list">
					<li>Incorporation’s year</li>
					<li>Type of Broker</li>
					<li>Account Type</li>
					<li>Paisa Power Classic</li>
					<li>Supported Exchanges</li>
					<li>Member of (NSDL/CDSL)</li>
				</ul>
			</div> <!-- .features -->
			
			<div class="cd-products-wrapper">
				<ul class="cd-products-columns">
					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Broker Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							<li>47.6 inches</li>
							<li>800Hz</li>
						</ul>
					</li> <!-- .product -->

					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Brokers Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							<li>47.6 inches</li>
							<li>800Hz</li>
						</ul>
					</li> <!-- .product -->

					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Brokers Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							<li>47.6 inches</li>
							<li>800Hz</li>
						</ul>
					</li> <!-- .product -->

				</ul> <!-- .cd-products-columns -->
			</div> <!-- .cd-products-wrapper -->
			
			</div> <!-- .cd-products-table -->
	</section> <!-- .cd-products-comparison-table -->	<section class="cd-products-comparison-table container">
		<header>
			<h2>Platforms</h2>
		</header>

		<div class="cd-products-table">
			<div class="features">
				<div class="top-info">Discount Brokers Compare</div>
				<ul class="cd-features-list">
					<li>Incorporation’s year</li>
					<li>Type of Broker</li>
					<li>Account Type</li>
					<li>Paisa Power Classic</li>
					
				</ul>
			</div> <!-- .features -->
			
			<div class="cd-products-wrapper">
				<ul class="cd-products-columns">
					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Broker Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							
						</ul>
					</li> <!-- .product -->

					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Brokers Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							
						</ul>
					</li> <!-- .product -->

					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Brokers Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							
						</ul>
					</li> <!-- .product -->

				</ul> <!-- .cd-products-columns -->
			</div> <!-- .cd-products-wrapper -->
			
			</div> <!-- .cd-products-table -->
	</section> <!-- .cd-products-comparison-table -->	<section class="cd-products-comparison-table container">
		<header>
			<h2>Charting</h2>	
		</header>

		<div class="cd-products-table">
			<div class="features">
				<div class="top-info">Discount Brokers Compare</div>
				<ul class="cd-features-list">
					<li>Incorporation’s year</li>
					<li>Type of Broker</li>
					<li>Account Type</li>
					
				</ul>
			</div> <!-- .features -->
			
			<div class="cd-products-wrapper">
				<ul class="cd-products-columns">
					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Broker Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							
						</ul>
					</li> <!-- .product -->

					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Brokers Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
						</ul>
					</li> <!-- .product -->

					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Brokers Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							
						</ul>
					</li> <!-- .product -->

				</ul> <!-- .cd-products-columns -->
			</div> <!-- .cd-products-wrapper -->
			
			</div> <!-- .cd-products-table -->
	</section> <!-- .cd-products-comparison-table -->	<section class="cd-products-comparison-table container">
		<header>
			<h2>Brokerages plan</h2>	
		</header>

		<div class="cd-products-table">
			<div class="features">
				<div class="top-info">Discount Brokers Compare</div>
				<ul class="cd-features-list">
					<li>Incorporation’s year</li>
					<li>Type of Broker</li>
					<li>Account Type</li>
					
				</ul>
			</div> <!-- .features -->
			
			<div class="cd-products-wrapper">
				<ul class="cd-products-columns">
					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Broker Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							
						</ul>
					</li> <!-- .product -->

					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Brokers Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							
						</ul>
					</li> <!-- .product -->

					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Brokers Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							
						</ul>
					</li> <!-- .product -->

				</ul> <!-- .cd-products-columns -->
			</div> <!-- .cd-products-wrapper -->
			
			</div> <!-- .cd-products-table -->
	</section> <!-- .cd-products-comparison-table -->	<section class="cd-products-comparison-table container">
		<header>
			<h2>Reporting</h2>

	
		</header>

		<div class="cd-products-table">
			<div class="features">
				<div class="top-info">Discount Brokers Compare</div>
				<ul class="cd-features-list">
					<li>Incorporation’s year</li>
					<li>Type of Broker</li>
					<li>Account Type</li>
					<li>Paisa Power Classic</li>
			    </ul>
			</div> <!-- .features -->
			
			<div class="cd-products-wrapper">
				<ul class="cd-products-columns">
					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Broker Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							
						</ul>
					</li> <!-- .product -->

					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Brokers Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							
						</ul>
					</li> <!-- .product -->

					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Brokers Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							
						</ul>
					</li> <!-- .product -->

				</ul> <!-- .cd-products-columns -->
			</div> <!-- .cd-products-wrapper -->
			
			</div> <!-- .cd-products-table -->
	</section> <!-- .cd-products-comparison-table -->	<section class="cd-products-comparison-table container">
		<header>
			<h2>Feature/ Convenience/ Support & Tools</h2>
		</header>

		<div class="cd-products-table">
			<div class="features">
				<div class="top-info">Discount Brokers Compare</div>
				<ul class="cd-features-list">
					<li>Incorporation’s year</li>
					<li>Type of Broker</li>
					<li>Account Type</li>
					<li>Paisa Power Classic</li>
					<li>Supported Exchanges</li>
					<li>Member of (NSDL/CDSL)</li>
					<li>Incorporation’s year</li>
					<li>Type of Broker</li>
					<li>Account Type</li>
					<li>Paisa Power Classic</li>
					<li>Supported Exchanges</li>
					<li>Member of (NSDL/CDSL)</li>
					<li>Incorporation’s year</li>
					<li>Type of Broker</li>
					<li>Account Type</li>
					<li>Paisa Power Classic</li>
					<li>Supported Exchanges</li>
				</ul>
			</div> <!-- .features -->
			
			<div class="cd-products-wrapper">
				<ul class="cd-products-columns">
					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Broker Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							<li>47.6 inches</li>
							<li>800Hz</li>
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							<li>47.6 inches</li>
							<li>800Hz</li>
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							<li>47.6 inches</li>
						</ul>
					</li> <!-- .product -->

					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Brokers Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							<li>47.6 inches</li>
							<li>800Hz</li>
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>47.6 inches</li>
							<li>800Hz</li>
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							<li>47.6 inches</li>
							<li>800Hz</li>
						</ul>
					</li> <!-- .product -->

					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Brokers Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							<li>47.6 inches</li>
							<li>800Hz</li>
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							<li>47.6 inches</li>
							<li>800Hz</li>
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							<li>47.6 inches</li>
							
						</ul>
					</li> <!-- .product -->

				</ul> <!-- .cd-products-columns -->
			</div> <!-- .cd-products-wrapper -->
			
			</div> <!-- .cd-products-table -->
	</section> <!-- .cd-products-comparison-table -->	
<section class="cd-products-comparison-table container">
		<header>
			<h2>Investment Options</h2>	
		</header>

		<div class="cd-products-table">
			<div class="features">
				<div class="top-info">Discount Brokers Compare</div>
				<ul class="cd-features-list">
					<li>Incorporation’s year</li>
					<li>Type of Broker</li>
					<li>Account Type</li>
					<li>Paisa Power Classic</li>
					<li>Supported Exchanges</li>
					<li>Member of (NSDL/CDSL)</li>
					<li>Supported Exchanges</li>
					<li>Member of (NSDL/CDSL)</li>
				</ul>
			</div> <!-- .features -->
			
			<div class="cd-products-wrapper">
				<ul class="cd-products-columns">
					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Broker Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							<li>47.6 inches</li>
							<li>800Hz</li>
							<li>47.6 inches</li>
							<li>800Hz</li>
						</ul>
					</li> <!-- .product -->

					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Brokers Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							<li>47.6 inches</li>
							<li>800Hz</li>
							<li>47.6 inches</li>
							<li>800Hz</li>
						</ul>
					</li> <!-- .product -->

					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Brokers Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							<li>47.6 inches</li>
							<li>800Hz</li>
							<li>47.6 inches</li>
							<li>800Hz</li>
						</ul>
					</li> <!-- .product -->

				</ul> <!-- .cd-products-columns -->
			</div> <!-- .cd-products-wrapper -->
			
			</div> <!-- .cd-products-table -->
	</section> <!-- .cd-products-comparison-table -->
	<section class="cd-products-comparison-table container">
		<header>
			<h2>Customer Support</h2>	
		</header>

		<div class="cd-products-table">
			<div class="features">
				<div class="top-info">Discount Brokers Compare</div>
				<ul class="cd-features-list">
					<li>Incorporation’s year</li>
					<li>Type of Broker</li>
					<li>Account Type</li>
					<li>Paisa Power Classic</li>
					<li>Supported Exchanges</li>
					<li>Member of (NSDL/CDSL)</li>
				</ul>
			</div> <!-- .features -->
			
			<div class="cd-products-wrapper">
				<ul class="cd-products-columns">
					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Broker Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							<li>47.6 inches</li>
							<li>800Hz</li>
						</ul>
					</li> <!-- .product -->

					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Brokers Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							<li>47.6 inches</li>
							<li>800Hz</li>
						</ul>
					</li> <!-- .product -->

					<li class="product">
						<div class="top-info">
							<img src="/img/img/2.jpg" alt="product image">
							<h3>Brokers Name</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li>$600</li>
							<li>Full service Broker</li>
							<li>1080p</li>
							<li>LED</li>
							<li>47.6 inches</li>
							<li>800Hz</li>
						</ul>
					</li> <!-- .product -->

				</ul> <!-- .cd-products-columns -->
			</div> <!-- .cd-products-wrapper -->
			
			</div> <!-- .cd-products-table -->
	</section> <!-- .cd-products-comparison-table -->